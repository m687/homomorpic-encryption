from IBM_HElib import *

# Generate a public/private key pair
public_key, private_key = generate_key_pair()

# Encrypt a message using the public key
encrypted_message = encrypt("Hello, world!", public_key)

# Decrypt the encrypted message using the private key
decrypted_message = decrypt(encrypted_message, private_key)

# Print the decrypted message
print(decrypted_message)
